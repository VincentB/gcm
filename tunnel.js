var exec = require('child_process').exec;
var logger = require('./log.js');
var utils = require('./utils.js');

var publicPort;
var tunnelingPort;
var receiverUser;
var publicIp;
var interval = null;
var tunnelStatus = "Tunnel not started";

var setupComplete = false;

var TUNNEL_TIMEOUT = 30 * 60 * 1000;

module.exports = {
    setupTunnel: function (tp, homeFolder) {
        tunnelingPort = tp;
        publicPort = "22";
        receiverUser = "user";
        publicIp = "127.0.0.1";
        setupComplete = true;
        utils.addToKnownHosts(homeFolder, publicIp, publicPort, function () {
            setupPeriodicTunnelCheck();
        })
    },
    getTunnelStatus: function () {
        return tunnelStatus;
    }
};

function setupPeriodicTunnelCheck() {
    if (interval == null) {
        interval = setInterval(startTunnel, TUNNEL_TIMEOUT);
        startTunnel();
    }
}

/**
 * Start the reverse ssh tunnel. Allowing the server to ssh into this machine
 */
function startTunnel() {
    if (!setupComplete) {
        logger.warning("Can not start tunnel. Setup not yet complete");
        return;
    }
    tunnelStatus = "Starting tunnel at " + new Date() + ". Public port: " + publicPort + ". TunnelingPort: " + tunnelingPort;
    logger.info(tunnelStatus);
    exec("ssh -p " + publicPort + " -N -R " + tunnelingPort + ":localhost:22 " + receiverUser + "@" + publicIp, function (error, output, stderr) {
        if (error)
            logger.error("Could not start tunnel. Error: " + error + ", " + stderr);
        else
            logger.info("tunnel command output: " + output + "\n");
    });
}

var fs = require('fs');

var ERROR = "ERROR";
var WARNING = "WARNING";
var INFO = "INFO";

module.exports = {
    info: function (text) {
        log(INFO, text);
    },
    warning: function (text) {
        log(WARNING, text);
    },
    error: function (text) {
        log(ERROR, text);
    }
};

function log(tag, text) {
    var date = new Date();
    var fullText = new Date() + ' [' + tag + '] ' + text + "\n";
    fs.appendFile('log' + date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate() + '.txt'
        , fullText
        , function (err) {
            if (err)
                console.log("Logging failed: " + err);
        });
    console.log(fullText);
}
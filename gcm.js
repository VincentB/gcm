#!/usr/bin/env node

/* gcm.js */
var ssh = require('./ssh.js');
var logger = require('./log.js');
var request = require('request');
var utils = require('./utils');
var systemd = require('./systemd.js');
var tunnel = require('./tunnel.js');

var getGatewayInfoInterval = null;

var sshKey;
var userName;
var hostname;
var homeFolder;
var gatewayId;
var jwt;

var CONFIG = require('./config.json');

//-----APP STARTUP
start();

/**
 * Start the application.
 * First get the UNIX username and hostname.
 * Second setup the crontabs if needed.
 * Third setup ssh keys if needed.
 * After the ssh is setup the configuration polling will start.
 */
function start() {
    utils.getUserName(function (un) {
        userName = un;
        utils.getHostName(function (hn) {
            hostname = hn;
            if (userName == undefined || hostname == undefined) {
                logger.error("Failed to get username and hostname");
                process.exit();
            }
            gatewayId = utils.getGatewayId();
            homeFolder = utils.getHomeFolder(userName);
            logger.info("Home folder is: " + homeFolder);
            systemd.checkSystemService(homeFolder, userName);
            setupSSH();
            startRebootTimer();
        });
    });
}

function setupSSH() {
    ssh.checkSSHkeys(homeFolder, userName, hostname, function (exists) {
        if (!exists) {
            setTimeout(setupSSH, 30 * 1000);
            logger.info("Failed to create ssh keys. Trying again in 30 seconds.")
        } else
            startGatewayInfoPolling();
    });
}

function startGatewayInfoPolling() {
    sshKey = ssh.getPublicKey(homeFolder);
    loginAndStart();
}

function loginAndStart() {
    apiLogin(function () {
        getGatewayInfo();
        if (getGatewayInfoInterval != null) {
            clearInterval(getGatewayInfoInterval);
            getGatewayInfoInterval = null;
        }
        getGatewayInfoInterval = setInterval(getGatewayInfo, 30 * 60000)
    });
}

function startRebootTimer(){
    setInterval(function () {
        var currentHour = new Date().getHours();
        if(currentHour === 13)
            utils.rebootNow();
    }, 60 * 60 * 1000);
}

//-----SERVER COMMUNICATION
/**
 * Login to the backend
 */
function apiLogin(onLoggedIn) {
    request({
        headers: {
            "username": CONFIG.user,
            "password": CONFIG.password,
            "merchantId": CONFIG.merchantId
        },
        uri: CONFIG.baseUrl + '/webapi/login',
        method: 'GET'
    }, function (err, res, body) {
        if (err) {
            logger.error("Api login failed: " + err);
            setTimeout(loginAndStart, 60 * 1000);
        } else {
            console.log("Api login response: \n" + body);
            jwt = JSON.parse(body).extra;
            onLoggedIn();
        }
    });
}


/**
 * Get the gateway info from the server
 */
function getGatewayInfo() {
    if (jwt == undefined) {
        loginAndStart();
        return;
    }
    var tunnelstatus = Buffer.from(tunnel.getTunnelStatus()).toString('base64');
    request({
        headers: {
            "Session": jwt,
            'tunnelStatus': tunnelstatus,
            'sshKey': Buffer.from(sshKey).toString('base64')
        },
        uri: CONFIG.baseUrl + '/productionapi/' + CONFIG.merchantId + '/gatewayinfo/' + gatewayId,
        method: 'GET'
    }, function (err, res, body) {
        if (err) {
            logger.error("Get request failed: " + err);
            loginAndStart();
        } else {
            console.log("Response body: \n" + body);
            handleGatewayInfoResponse(JSON.parse(body));
            utils.setGatewayTime(res.headers.currenttimestamp);
        }
    });
}

/**
 * Handles the flags set in the response
 * @param response JSON object
 */
function handleGatewayInfoResponse(response) {
    if (response.rebootFlag)
        utils.setRebootRequired();
    if (response.serverSshKey)
        ssh.saveTunnelServerKey(homeFolder, response.serverSshKey);

    tunnel.setupTunnel(response.tunnelPort, homeFolder);
    utils.setCheckActionsInterval()
}


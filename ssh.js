var exec = require('child_process').exec;
var fs = require('fs');
var logger = require('./log.js');
var utils = require('./utils.js');

module.exports = {
    /**
     * Check if a ssh keys exists
     * If not generate one
     */
    checkSSHkeys: function (homeFolder, userName, hostName, callback) {
        fs.readFile(homeFolder + '/.ssh/id_rsa.pub', 'utf8', function (err, contents) {
            if (err || contents == undefined || contents.length < 1) {
                generateSSHKeys(homeFolder, userName, hostName, callback);
            } else {
                logger.info("SSH keys already exist.");
                callback(true);
            }
        });
    },
    /**
     * Get the public SSH key from file
     */
    getPublicKey: function (homeFolder) {
        return utils.trimString(fs.readFileSync(homeFolder + '/.ssh/id_rsa.pub', 'utf8'));
    },
    /**
     * Save the ssh keys retrieved from the backend to file.
     * Synchronised
     * @param{string} homeFolder path to the homefolder
     * @param{string} key tunnel server public key
     */
    saveTunnelServerKey(homeFolder, key) {
        utils.addAction(utils.ACTION_ADD_TO_AUTH);
        var keyFile = homeFolder + '/.ssh/authorized_keys';
        var authorizedKeys = fs.readFileSync(keyFile, { flag: 'a+' });
        if (authorizedKeys.indexOf(key) === -1) {
            logger.info("Adding tunnel server key to authorized keys");
            fs.appendFileSync(keyFile, key + '\n');
        }
        utils.setActionCompleted(utils.ACTION_ADD_TO_AUTH);
    }
};


/**
 * Generate a ssh key without a passphrase for this user and device.
 */
function generateSSHKeys(homeFolder, userName, hostName, callback) {
    exec('cat /dev/zero | ssh-keygen -q -N "" -t rsa -C ' + utils.trimString(userName) + '@' + utils.trimString(hostName) + ' -f ' + homeFolder + '/.ssh/id_rsa', function (error) {
        if (error) {
            callback(false);
            logger.error("Creating SSH KEYS FAILED: " + error);
        } else {
            callback(true);
            logger.info("Created ssh keys")
        }
    });
}
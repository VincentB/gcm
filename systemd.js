var exec = require('child_process').exec;
var logger = require('./log.js');
var fs = require('fs');

module.exports = {
    /**
     * Check if a system service for gcm exists and is enabled. If not creates and enables it.
     * @param homeFolder the home folder of the current user
     * @param userName the username of the current user
     */
    checkSystemService: function (homeFolder, userName) {
        exec('systemctl list-unit-files | grep gcm', function (error, output, stderr) {
            if (error)
                logger.warning("System service not found not found: " + error + ', ' + stderr);
            if (error || output == undefined || output.length < 1)
                createSystemService(homeFolder, userName);
            else if (output.indexOf("enabled") === -1)
                enableService();
            else
                logger.info("System service already exists")
        });
    }
};

/**
 * Enable the gcm system service
 */
function enableService() {
    exec('systemctl enable gcm.service', function (error, output, stderr) {
        if (error)
            logger.error("Error enabling system service: " + error + ', ' + stderr);
        else {
            logger.info("Service enabled. Starting service");
            exec('systemctl start gcm.service', function (error, output, stderr) {
                if (error)
                    logger.error("Error starting system service: " + error + ', ' + stderr);
            })
        }
    })
}

/**
 * Create a system service file from the template and enable the service
 * @param homeFolder
 * @param userName
 */
function createSystemService(homeFolder, userName) {
    var serviceDefinition = fs.readFileSync(__dirname + "/servicedefinition.def");
    var s1 = serviceDefinition.toString();
    var s2 = s1.replace(new RegExp("#HOME_FOLDER#", 'g'), homeFolder).toString()
        .replace(new RegExp("#USERNAME#", 'g'), userName).toString();
    console.log("service definition: \n" + s2 + "\n");
    fs.writeFileSync('/etc/systemd/system/gcm.service', s2);
    enableService();
}


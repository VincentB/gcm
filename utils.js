var logger = require('./log.js');
var exec = require('child_process').exec;
var fs = require('fs');
var decomment = require('decomment');

var actionsCompleted = {};
var rebootRequired = false;
var actionCheckInterval = null;
var gatewayType;

var TYPE_LORANK8 = "TYPE_LORANK8";
var TYPE_MULTITECH = "TYPE_MULTITECH";

var configPaths = {
    TYPE_MULTITECH: '/var/config/lora/local_conf.json', //Multitech
    TYPE_LORANK8: '/root/lorank8v1/local_conf.json'   //Lorank8
};

module.exports = {
    /**
     * Removes all break lines and trims string
     */
    trimString: function (string) {
        return string.replace(/(\r\n\t|\n|\r\t)/gm, "").trim();
    },
    /**
     * Gets the UNIX username of the current user by executing the whoami command
     * @param callback with the UNIX username as parameter
     */
    getUserName: function (callback) {
        exec('whoami', function (error, username) {
            if (error)
                logger.error("Failed getting username: " + error);
            else
                callback(module.exports.trimString(username));
        });
    },
    /**
     * Gets the device name by executing the hostname command
     * @param callback with the device name as parameter
     */
    getHostName: function (callback) {
        exec('hostname', function (error, hostname) {
            if (error)
                logger.error("Failed getting hostname" + error);
            else
                callback(module.exports.trimString(hostname));
        });
    },
    /**
     * Set gateway time base on millisecond timestamp
     * if the difference between timestamps is bigger than a minute
     * @param timestamp in milliseconds
     */
    setGatewayTime: function (timestamp) {
        if ((Date.now() - timestamp) > 60000)
            exec('date -s \"' + getDateString(timestamp) + '\"', function (error) {
                if (error)
                    logger.error("Error setting time: " + error);
                logger.info("Changed gateway time")
            })
    },
    /**
     * @param username the UNIX username of this device
     * @returns {string} the path to the home folder.
     */
    getHomeFolder(username) {
        logger.info("Current gateway type is: " + gatewayType);
        switch (gatewayType) {
            case TYPE_LORANK8:
                return '/' + username;
            case TYPE_MULTITECH:
                return '/home/' + username;
        }
    },
    /**
     * Gets the lora gateway id from the local_conf.json file
     * Checks multiple locations for different types of gateway
     * @returns {string} the gatewayId
     */
    getGatewayId: function () {
        var localConfig;
        for (var key in configPaths) {
            if (fs.existsSync(configPaths[key])) {
                localConfig = fs.readFileSync(configPaths[key], 'utf8');
                gatewayType = key;
                break;
            }
        }

        return "eui-" + JSON.parse(decomment(localConfig)).gateway_conf.gateway_ID.toLowerCase();
    },
    /**
     * Set flag that a reboot is required after all pending actions have been completed
     */
    setRebootRequired: function () {
        rebootRequired = true;
    },
    /**
     * Add an action to the pending actions.
     * Actions are used to check if all jobs are finished and the system can reboot
     * @param{string} action
     */
    addAction: function (action) {
        actionsCompleted[action] = false;
    },
    /**
     * Execute an immediate reboot
     */
    rebootNow : function(){
        reboot();
    },
    /**
     * Indicate that a pending action has been completed
     * Actions are used to check if all jobs are finished and the system can reboot
     * @param{string} action
     */
    setActionCompleted: function (action) {
        actionsCompleted[action] = true;
    },
    /**
     * Start checking if all pending actions have been completed every 0.5 seconds
     */
    setCheckActionsInterval: function () {
        clearActionCheckInterval();
        actionCheckInterval = setInterval(checkActionsCompleted, 500);
    },
    addToKnownHosts(homeFolder, ip, port, callback) {
        exec("ssh-keyscan -H -p " + port + " " + ip + " >> " + homeFolder + "/.ssh/known_hosts", function (error, output, stderr) {
            if (error)
                logger.error("Failed to add ip: " + ip + " to known hosts. " + error + ", " + stderr + ", " + output);
            else {
                logger.info("Keyscan complete. " + output);
                callback();
            }
        })
    },
    /**
     * All available actions
     */
    ACTION_REPLACE_GCM: "replaceGcm",
    ACTION_ADD_TO_AUTH: "addAuth"
};

/**
 * Reboot the device
 */
function reboot() {
    exec('sudo reboot', function (error) {
        if (error)
            logger.error("Error executing reboot command: " + error);
        else
            logger.info("Rebooting device");
    });
}

function isRebootRequired() {
    return rebootRequired;
}


function clearActions() {
    actionsCompleted = {};
}

function clearActionCheckInterval() {
    if (actionCheckInterval != null) {
        clearInterval(actionCheckInterval);
        actionCheckInterval = null;
    }
}

/**
 * Check if the list of pending actions have been completed
 */
function checkActionsCompleted() {
    var finished = true;
    for (var action in actionsCompleted) {
        if (actionsCompleted.hasOwnProperty(action)) {
            if (!actionsCompleted[action]) {
                finished = false;
            }
        }
    }
    if (finished) {
        var needReboot = isRebootRequired();
        logger.info("All required actions finished." + (needReboot ? " Reboot required." : ""));
        clearActions();
        clearActionCheckInterval();
        if (needReboot) {
            logger.info("Rebooting in 3 seconds.");
            setTimeout(reboot, 3000);
        }
    }
}

/**
 * Turns a timestamp in ms to a readable timestring. The timestring can be used to change the time via the command line
 * @param timestamp unix timestamp in ms
 * @returns {string} readable date/timestring
 */
function getDateString(timestamp) {
    var date = new Date(timestamp);
    return (date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate() + " " + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds());
}